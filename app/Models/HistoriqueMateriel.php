<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoriqueMateriel extends Model
{
    use HasFactory;
    protected $fillable = [
        'materielsId_historique',
        'quantity_initial',
        'quantity_nouvelle',
        'quantity_ajouter',
        'montant_payer',
        'action_materiel',
        'statut_materiel',
        'is_deleted'
    ];
}
