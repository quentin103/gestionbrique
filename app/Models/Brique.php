<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brique extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_brique',
        'description_brique',
        'type_brique',
        'image_brique',
        'is_deleted'
    ];
}
