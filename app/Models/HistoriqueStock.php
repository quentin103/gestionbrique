<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoriqueStock extends Model
{
    use HasFactory;
    protected $fillable = [
        'briquesStockId_historique',
        'quantity_initial',
        'quantity_nouvelle',
        'quantity_ajouter',
        'montant_payer',
        'action_stock',
        'statut_stock',
        'is_deleted'
    ];
}
