<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $input)
 */
class Machine extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_machine',
        'reference_machine',
        'name_machine',
        'description_machine',
        'image_machine',
        'type_machine',
        'fabricant_machine',
        'date_installation',
        'emplacement',
        'capacite_production_max',
        'status_machine',
        'is_deleted'
    ];

}
