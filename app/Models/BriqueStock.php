<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BriqueStock extends Model
{
    use HasFactory;
    protected $fillable = [
        'briquesId',
        'quantite_en_stock',
        'quantite_alert_stock',
        'prix_unitaire',
        'code_barre',
        'lieu_de_stockage',
        'is_deleted'
    ];
}
