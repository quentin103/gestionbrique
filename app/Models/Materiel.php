<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materiel extends Model
{
    use HasFactory;
    protected $fillable = [
        'numero_serie',
        'name_materiel',
        'fournisseurId',
        'fournisseur_tel',
        'type_materiel',
        'quantite_stock',
        'seuil_minimum',
        'unite',
        'image',
        'emplacement_stockage',
        'commentaire_utilisation',
        'is_deleted'
    ];
}
