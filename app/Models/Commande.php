<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $fillable = [
        'code_commande',
        'clientId',
        'date_commande',
        'montant_total',
        'modepaiementId',
        'adresse_livraison',
        'ville_livraison',
        'statut_commande',
        'is_deleted'
    ];
}
