<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommandeDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'commandeId',
        'produitId',
        'quantite',
        'prix_unitaire',
        'remise',
        'is_deleted'
    ];
}
