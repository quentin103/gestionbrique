<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fournisseur extends Model
{
    use HasFactory;
    protected $fillable = [
        'type_fournisseur',
        'name_fournisseur',
        'telephone1_fournisseur',
        'telephone2_fournisseur',
        'email_fournisseur',
        'adresse_fournisseur',
        'responsable_fournisseur',
        'responsable_tel_fournisseur',
        'is_deleted'
    ];
}
