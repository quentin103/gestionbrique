<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModePaiement extends Model
{
    use HasFactory;
    protected $fillable = [
        'name_mode_paiement',
        'information_mode_paiement',
        'statut_mode_paiement',
        'is_deleted'
    ];
}
