<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    use HasFactory;
    protected $fillable = [
        'machineId',
        'briquesId',
        'date_production',
        'heure_debut_production',
        'heure_fin_production',
        'nombre_brique',
        'nombre_brique_fissuree',
        'cout_production',
        'commentaire',
        'responsable_production',
        'responsable_tel_production',
        'status_production',
        'is_deleted'
    ];
}
