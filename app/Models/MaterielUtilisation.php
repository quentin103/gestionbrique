<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaterielUtilisation extends Model
{
    use HasFactory;
    protected $fillable = [
        'productionId',
        'materielId',
        'quantite_utilisee',
        'unite',
        'debut_utilisation',
        'fin_utilisation',
        'responsable_utilisation',
        'is_deleted'
    ];
}
