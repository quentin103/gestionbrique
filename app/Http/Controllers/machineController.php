<?php

namespace App\Http\Controllers;

use App\Http\Controllers\responseController as responseJson;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Machine;
use Exception;
use Validator;

class machineController extends Controller
{

    public function generateRandomString($length = 6): string
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZazertyuiopqsdfghjklmwxcvbn';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return 'M-'.$randomString;
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        try {
            $data = Machine::where('is_deleted', false)->get();
            return responseJson::responseSuccess($data, 'Machine index successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): Jsonresponse
    {
        try {
            $input = $request->all();
            $input['code_machine'] = $this->generateRandomString(10);
            $data = Machine::create($input);
            return responseJson::responseSuccess($data, 'Machine created successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id): JsonResponse
    {
        //
        try {
            $data = Machine::find($id);
            if (is_null($data)) {
                return responseJson::responseError('error', 'Machine not found', 404);
            }
            return responseJson::responseSuccess($data, 'Machine show successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): JsonResponse
    {
        //
        try {
            //modification d'une machine
            $input = $request->all();
            $machine = Machine::find($id);
            // Check if the machine exists
            if (is_null($machine)) {
                return responseJson::responseError('error', 'Machine not found', 404);
            }
            // Modify the machine here
            $machine->update($input);
            $machine->save();
            // Return the modified machine
            return responseJson::responseSuccess($machine, 'Machine modified successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        // supprimer une machine
        try {
            $machine = Machine::where('id', $id)->where('is_deleted', false)->first();
            if (is_null($machine)) {
                return responseJson::messageError('Machine not found', 404);
            }
            $machine->is_deleted = true;
            $machine->save();
            return responseJson::messageSuccess( 'Machine deleted successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }


}
