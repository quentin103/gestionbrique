<?php

namespace App\Http\Controllers;

use App\Http\Controllers\responseController as responseJson;
use App\Models\Brique;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Machine;
use App\Models\Production;
use Exception;
use Validator;

class productionController extends Controller
{
    public function  indexProduction(): JsonResponse
    {
        //
        try {
            $data = Production::where('is_deleted', false)->get();
            return responseJson::responseSuccess($data, 'Production retrieved successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }
   public function storeProduction(Request $request): JsonResponse
   {
       try {
           $input = $request->all();
           $machine = Machine::find($input['machineId']);
           if (is_null($machine)) {
               return responseJson::messageError( 'Machine not found');
           }
           //verification brique
           $brique = Brique::find($input['machineId']);
           if(is_null($brique)){
               return responseJson::messageError( 'Brique not found');
           }
           $production = Production::create($input);
           return responseJson::responseSuccess($production, 'Production created successfully.');
       } catch (Exception $e) {
           return responseJson::responseError('error', $e->getMessage(), 500);
       }

   }
   public function  updateProduction(Request $request, string $id): JsonResponse
   {
       //
       try {
           //modification d'une machine
           $input = $request->all();
           $machine = Production::find($id);
           // Check if the machine exists
           if (is_null($machine)) {
               return responseJson::responseError('error', 'Production not found', 404);
           }
           // Modify the machine here
           $machine->update($input);
           $machine->save();
           // Return the modified machine
           return responseJson::responseSuccess($machine, 'Production modified successfully.');
       } catch (Exception $e) {
           return responseJson::responseError('error', $e->getMessage(), 500);
       }
   }

   public function  showProduction(string $id): JsonResponse
   {
       //
       try {
           $data = Production::find($id);
           if (is_null($data)) {
               return responseJson::responseError('error', 'Production not found', 404);
           }
           return responseJson::responseSuccess($data, 'Production show successfully.');
       } catch (Exception $e) {
           return responseJson::responseError('error', $e->getMessage(), 500);
       }
   }

   public function destroyProduction(string $id): JsonResponse
   {
       //
       try {
           $machine = Production::where('id', $id)->where('is_deleted', false)->first();
           if (is_null($machine)) {
               return responseJson::messageError('Production not found', 404);
           }
           $machine->is_deleted = true;
           $machine->save();
           return responseJson::messageSuccess('Production deleted successfully.');
       } catch (Exception $e) {
           return responseJson::responseError('error', $e->getMessage(), 500);
       }
   }
}
