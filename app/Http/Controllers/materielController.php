<?php

namespace App\Http\Controllers;

use App\Http\Controllers\responseController as responseJson;
use App\Models\HistoriqueMateriel;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Materiel;
use Exception;
use Validator;

class materielController extends Controller
{
    public function generateRandomString($length = 8): string
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return 'EQ-' . $randomString;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        try {
            $data = Materiel::where('is_deleted', false)->get();
            return responseJson::responseSuccess($data, 'Materiel index successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        try {
            // ajouter une machine
            $validator = Validator::make($request->all(), [
                'name_materiel' => 'required',
            ], [
                'name_materiel.required' => 'name_materiel is required',
            ]);
            $input = $request->all();
            if ($validator->fails()) {
                return responseJson::responseError('Validation Error', $validator->errors(), 422);
            }
            $input['numero_serie'] = $this->generateRandomString();
            $data = Materiel::create($input);
            return responseJson::responseSuccess($data, 'Materiel created successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): JsonResponse
    {
        //
        try {
            $data = Materiel::find($id);
            // historique materiel
            $data->historique_materiel_sortie = HistoriqueMateriel::where('materielsId_historique', $id)->where('is_deleted', false)->where('action_materiel', 'SORTIE')->orderBy('created_at', 'desc')->get();
            $data->historique_materiel_entrer = HistoriqueMateriel::where('materielsId_historique', $id)->where('is_deleted', false)->where('action_materiel', 'ENTRER')->orderBy('created_at', 'desc')->get();
            $data->historique_materiel = HistoriqueMateriel::where('materielsId_historique', $id)->where('is_deleted', false)->orderBy('created_at', 'desc')->get();

            if (is_null($data)) {
                return responseJson::responseError('error', 'Materiel not found', 404);
            }
            return responseJson::responseSuccess($data, 'Materiel show successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id): JsonResponse
    {
        //
        try {
            //modification d'une machine
            $input = $request->all();
            $machine = Materiel::find($id);
            // Check if the machine exists
            if (is_null($machine)) {
                return responseJson::responseError('error', 'Materiel not found', 404);
            }
            // Modify the machine here
            $machine->update($input);
            $machine->save();
            // Return the modified machine
            return responseJson::responseSuccess($machine, 'Materiel modified successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): JsonResponse
    {
        //
        try {
            $machine = Materiel::where('id', $id)->where('is_deleted', false)->first();
            if (is_null($machine)) {
                return responseJson::messageError('Materiel not found', 404);
            }
            $machine->is_deleted = true;
            $machine->save();
            return responseJson::messageSuccess('Materiel deleted successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function sortieEntrerMateriel_Store(Request $request, string $id): JsonResponse
    {
        //
        try {
            $validator = Validator::make($request->all(), [
                'action_materiel' => 'required',
                'statut_materiel' => 'required',
            ], [
                'action_materiel.required' => 'action materiel is required',
                'statut_materiel.required' => 'statut materiel is required',
            ]);
            $input = $request->all();
            if ($validator->fails()) {
                return responseJson::responseError('Validation Error', $validator->errors(), 422);
            }
            $machine = Materiel::where('id', $id)->where('is_deleted', false)->first();
            if (is_null($machine)) {
                return responseJson::messageError('Materiel not found', 404);
            }
            if ($input['action_materiel'] == 'SORTIE' && $input['quantity_ajouter']) {
                // condition sortie materiel
                //|> Vérification de la quantité de stock
                if ($machine->quantite_stock < $input['quantity_ajouter']) {
                    return responseJson::messageError('La quantité de stock est insuffisante, quantité en stock est : ' . $machine->quantite_stock . '', 404);
                }
                $input['statut_materiel'] = 'VALIDER';
                $input['quantity_initial'] = $machine->quantite_stock;
                $input['quantity_nouvelle'] = $machine->quantite_stock - $input['quantity_ajouter'];
                // Sortie d'un materiel
                Materiel::find($id)->update([
                    'quantite_stock' => $input['quantity_initial'] - $input['quantity_ajouter'],
                ]);
            } elseif ($input['action_materiel'] == 'ENTRER') {
                // condition entrer materiel
                $input['statut_materiel'] = 'VALIDER';
                $input['quantity_initial'] = $machine->quantite_stock;
                $input['quantity_nouvelle'] = $machine->quantite_stock + $input['quantity_ajouter'];

            }
            $data = HistoriqueMateriel::create($input);
            return responseJson::responseSuccess($data, 'Creation effectuée avec susccess');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function sortieEntrerMateriel_index(): JsonResponse
    {
        //
        try {
            $data = Materiel::where('is_deleted', false)->get();

            // Recuperation des informations du materiel
            foreach ($data as $key => $value) {
                $data[$key]['historique_materiel_sortie'] = HistoriqueMateriel::where('materielsId_historique', $value->id)->where('is_deleted', false)->where('action_materiel', 'SORTIE')->orderBy('created_at', 'desc')->get();
                $data[$key]['historique_materiel_entrer'] = HistoriqueMateriel::where('materielsId_historique', $value->id)->where('is_deleted', false)->where('action_materiel', 'ENTRER')->orderBy('created_at', 'desc')->get();
                $data[$key]['historique_materiel'] = HistoriqueMateriel::where('materielsId_historique', $value->id)->where('is_deleted', false)->orderBy('created_at', 'desc')->get();
            }

            return responseJson::responseSuccess($data, 'Materiel sortie et entrer all successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function sortieEntrerMateriel_Destroy(string $id): JsonResponse
    {
        //
        try {
            //modification d'une machine
            $data =  HistoriqueMateriel::where('id', $id)->where('is_deleted', false)->first();
            // Check if the machine exists
            if (is_null($data)) {
                return responseJson::responseError('error', 'historique one not found', 404);
            }
            $data =  HistoriqueMateriel::find($id);
            // redution de la quantité du materiel
            $materiel = Materiel::find($data->materielsId_historique);
            // verifier la quantité
            if($data->action_materiel == 'SORTIE'){
                $materiel->quantite_stock = $materiel->quantite_stock + $data->quantity_ajouter;
            }else if($data->action_materiel == 'ENTRER'){
                $materiel->quantite_stock = $materiel->quantite_stock - $data->quantity_ajouter;
            }
            // Update the matériel here
            $materiel->save();
            // Delete the historique one
            $data->is_deleted = true;
            $data->save();
            // Return the modified machine
            return responseJson::responseSuccess($materiel, "Suppression effectuée avec susccess");
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

}
