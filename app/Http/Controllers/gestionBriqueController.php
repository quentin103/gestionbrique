<?php

namespace App\Http\Controllers;

use App\Http\Controllers\responseController as responseJson;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Brique;
use App\Models\BriqueStock;
use Exception;
use Validator;

class gestionBriqueController extends Controller
{

    // Brique
    public function storeBrique(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'name_brique' => 'required|unique:briques',
                'type_brique' => 'required'
            ], [
                'name_brique.required' => 'Name brique is required',
                'name_brique.unique' => 'Name brique is already exists',
                'type_brique.required' => 'Type brique is required',
            ]);
            if ($validator->fails()) {
                return responseJson::responseError('Validation Error', $validator->errors(), 422);
            }
            $input = $request->all();
            $data = Brique::create($input);
            return responseJson::responseSuccess($data, 'Brique created successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function updateBrique(Request $request, string $id): JsonResponse
    {
        //
        try {
            //modification d'une machine
            $input = $request->all();
            $brique = Brique::find($id);
            // Check if the machine exists
            if (is_null($brique)) {
                return responseJson::responseError('error', 'Brique not found', 404);
            }
            // Modify the machine here
            $brique->update($input);
            $brique->save();
            // Return the modified machine
            return responseJson::responseSuccess($brique, 'Brique modified successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function destroyBrique(string $id): JsonResponse
    {
        // supprimer une machine
        try {
            $brique = Brique::where('id', $id)->where('is_deleted', false)->first();
            if (is_null($brique)) {
                return responseJson::messageError('Brique not found', 404);
            }
            $brique->is_deleted = true;
            $brique->save();
            return responseJson::messageSuccess( 'Brique deleted successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function indexBrique(): JsonResponse
    {
        //
        try {
            $data = Brique::where('is_deleted', false)->get();
            return responseJson::responseSuccess($data, 'Brique index successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function showBrique(String $id): JsonResponse
    {
        //
        try {
            $data = Brique::find($id);
            return responseJson::responseSuccess($data, 'Brique show successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    // BriqueStock
    public function storeBriqueStock(Request $request): JsonResponse
    {
        try {
            $input = $request->all();
            $data = BriqueStock::where('briquesId',$input['briquesId'])->where('is_deleted', false)->first();
            if ($data){
                return responseJson::messageError('cette brique exists dans votre BriqueStock.');
            }
            $data = BriqueStock::create($input);
            return responseJson::responseSuccess($data, 'BriqueStock created successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function updateBriqueStock(Request $request, string $id): JsonResponse
    {
        //
        try {
            $input = $request->all();
            $brique = BriqueStock::where('id',$id)->where('is_deleted', false)->first();
            if(is_null($brique)){
                return responseJson::messageError('BriqueStock not found, non upadte', 404);
            }
            $brique = BriqueStock::find($id);
            $brique->update($input);
            $brique->save();
            // Return the modified machine
            return responseJson::responseSuccess($brique, 'BriqueStock modified successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function destroyBriqueStock(string $id): JsonResponse
    {
        // supprimer une machine
        try {
            $brique = BriqueStock::where('id', $id)->where('is_deleted', false)->first();
            if (is_null($brique)) {
                return responseJson::messageError('BriqueStock not found', 404);
            }
            $brique->is_deleted = true;
            $brique->save();
            return responseJson::messageSuccess( 'BriqueStock deleted successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function indexBriqueStock(): JsonResponse
    {
        //
        try {
            $data = BriqueStock::where('is_deleted', false)->get();
            foreach ($data as $key => $value){
                $data[$key]['brique'] = brique::where('id',$value['briquesId'])->first();
            }
            return responseJson::responseSuccess($data, 'BriqueStock index successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

    public function showBriqueStock(String $id): JsonResponse
    {
        //
        try {
            $data = BriqueStock::find($id);
            return responseJson::responseSuccess($data, 'Brique show successfully.');
        } catch (Exception $e) {
            return responseJson::responseError('error', $e->getMessage(), 500);
        }
    }

}
