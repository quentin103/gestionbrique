<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Jsonresponse;
use App\Http\Controllers\responseController as responseJson;
use App\Models\User;
use Exception;
use Validator;

class authController extends Controller
{
    public function login(Request $request): Jsonresponse
    {
        try {
            $validator = Validator::make($request->all(),  [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                return responseJson::responseError('Validation Error', $validator->errors());
            }

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
                $success = [];
                if (!empty($user->name)) {
                    $success['token'] =  $user->createToken($user->name . '_' . Carbon::now(), ['*'], Carbon::now()->addDay(7))->plainTextToken;
                    $success['name'] =  $user->name;
                    $success['id'] =  $user->id;
                }
                return responseJson::responseSuccess($success, 'User login successfully.');
            } else {
                return  responseJson::MessageError('incorrect login or password');
            }
        } catch (Exception $e) {
            return  responseJson::responseError('error', $e->getMessage(),400);
        }
    }


    public function detailUser(Request $request): Jsonresponse
    {
        try {
            $response =  $request->user();
            return responseJson::responseSuccess($response, 'User detail.');
        } catch (Exception $e) {
            return  responseJson::responseError('error', $e->getMessage(), 400);
        }
    }


    public function logout(Request $request): Jsonresponse
    {
        try {
            $response = $request->user()->currentAccessToken()->delete();
            return responseJson::responseSuccess($response, 'User logout successfully.');
        } catch (Exception $e) {
            return  responseJson::responseError('error', $e->getMessage(), 400);
        }
    }

    public function register(Request $request): Jsonresponse
    {
        try {

            $validator = Validator::make($request->all(),  [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'confirmpassword' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return responseJson::responseError('Validation Error', $validator->errors());
            }
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->plainTextToken;
            $success['name'] =  $user->name;

            return responseJson::responseSuccess($success, 'User register successfully.');

        } catch (Exception $e) {

            return  responseJson::responseError('error', $e->getMessage(),400);

        }
    }

    public function Unauthorized(): Jsonresponse
    {
        return responseJson::MessageError('You are not authorized',  400);
    }
}
