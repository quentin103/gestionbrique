<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use \Illuminate\Http\JsonResponse;
class responseController extends Controller
{
    //
    public static function responseSuccess($result, $message): JsonResponse
    {
        return response()->json([
            'statusCode' => 200,
            'data' => $result,
            'statusMessage' => $message,
        ], 200);
    }

    public static function responseError( $errorMessages,$errors = [],$code = 404):JsonResponse
    {
        $response = [
            'statusCode' => $code,
            'statusMessage' => $errorMessages,
        ];
        if ($errors) {
            $response['data'] =  $errors;
        }
        return response()->json($response, $code);
    }
    public static function messageSuccess($Messages = [], $code = 200):JsonResponse
    {
        return response()->json([
            'statusCode' => 200,
            'statusMessage' => $Messages,
        ], $code);
    }
    public static function messageError($Messages = [], $code = 404):JsonResponse
    {
        return response()->json([
            'statusCode' => 404,
            'statusMessage' => $Messages,
        ], $code);
    }
}
