<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materiel_utilisations', function (Blueprint $table) {
            $table->id();
            $table->integer('productionId');
            $table->integer('materielId');
            $table->double('quantite_utilisee');
            $table->string('unite')->nullable();
            $table->dateTime('debut_utilisation')->nullable();
            $table->dateTime('fin_utilisation')->nullable();
            $table->longText('responsable_utilisation')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('materiel_utilisations');
    }
};
