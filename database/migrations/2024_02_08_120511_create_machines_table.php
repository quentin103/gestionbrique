<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('machines', function (Blueprint $table) {
            $table->id();
            $table->string('code_machine')->unique();
            $table->string('reference_machine')->unique()->nullable();
            $table->string('name_machine');
            $table->longText('description_machine')->nullable();
            $table->longText('image_machine')->nullable();
            $table->string('type_machine')->nullable();
            $table->string('fabricant_machine')->nullable();
            $table->datetime('date_installation')->default(now())->nullable();
            $table->longText('emplacement')->nullable();
            $table->double('capacite_production_max')->nullable();
            $table->string('status_machine')->default('DISPONIBLE'); //['DISPONIBLE','ENCOURS_DE_PRODUCTION','EN_REPARATION']
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('machines');
    }
};
