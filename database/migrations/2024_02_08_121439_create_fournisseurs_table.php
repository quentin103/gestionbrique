<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('fournisseurs', function (Blueprint $table) {
            $table->id();
            $table->string('type_fournisseur');
            $table->string('name_fournisseur');
            $table->string('telephone1_fournisseur');
            $table->string('telephone2_fournisseur')->nullable();
            $table->string('email_fournisseur')->nullable();
            $table->longText('adresse_fournisseur')->nullable();
            $table->longText('responsable_fournisseur')->nullable();
            $table->string('responsable_tel_fournisseur')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('fournisseurs');
    }
};
