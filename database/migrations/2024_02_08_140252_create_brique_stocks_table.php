<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('brique_stocks', function (Blueprint $table) {
            $table->id();
            $table->integer('briquesId')->unique();
            $table->double('quantite_en_stock');
            $table->double('quantite_alert_stock')->nullable();
            $table->double('prix_unitaire')->nullable();
            $table->longText('code_barre')->nullable();
            $table->longText('lieu_de_stockage')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('brique_stocks');
    }
};
