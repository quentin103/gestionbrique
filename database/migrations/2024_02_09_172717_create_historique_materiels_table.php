<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('historique_materiels', function (Blueprint $table) {
            $table->id();
            $table->integer('materielsId_historique');
            $table->integer('quantity_initial');
            $table->integer('quantity_nouvelle');
            $table->integer('quantity_ajouter');
            $table->double('montant_payer');
            $table->enum('action_materiel',['SORTIE','ENTRER']);
            $table->enum('statut_materiel',['ANNULER','VALIDER'])->default('VALIDER');
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('historique_materiels');
    }
};
