<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('name_client');
            $table->longText('lastname_client');
            $table->longText('email_client')->nullable();
            $table->string('tel1_client');
            $table->string('tel2_client')->nullable();
            $table->longText('adresse_client')->nullable();
            $table->longText('ville_client')->nullable();
            $table->boolean('statut_client')->default(true);
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};
