<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->id();
            $table->integer('machineId');
            $table->integer('briquesId');
            $table->dateTime('date_production');
            $table->dateTime('heure_debut_production')->nullable();
            $table->dateTime('heure_fin_production')->nullable();
            $table->double('nombre_brique')->nullable();
            $table->double('nombre_brique_fissuree')->nullable();
            $table->double('cout_production')->nullable();
            $table->longText('commentaire')->nullable();
            $table->longText('responsable_production')->nullable();
            $table->string('responsable_tel_production')->nullable();
            $table->string('status_production')->default('EN_ATTENTE'); // EN_COURS, TERMINE OU ANNULE
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('productions');
    }
};
