<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('materiels', function (Blueprint $table) {
            $table->id();
            $table->longText('numero_serie')->nullable();
            $table->string('name_materiel');
            $table->string('fournisseurId')->nullable();
            $table->string('fournisseur_name')->nullable();
            $table->string('fournisseur_tel')->nullable();
            $table->longText('type_materiel')->nullable();
            $table->double('quantite_stock');
            $table->double('seuil_minimum')->nullable();
            $table->longText('unite')->nullable();
            $table->longText('image')->nullable();
            $table->longText('emplacement_stockage')->nullable();
            $table->longText('commentaire_utilisation')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('materiels');
    }
};
