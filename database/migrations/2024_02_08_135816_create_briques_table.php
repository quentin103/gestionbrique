<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('briques', function (Blueprint $table) {
            $table->id();
            $table->longText('name_brique');
            $table->longText('description_brique')->nullable();
            $table->string('type_brique')->nullable();
            $table->longText('image_brique')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('briques');
    }
};
