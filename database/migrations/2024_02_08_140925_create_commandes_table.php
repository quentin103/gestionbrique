<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->id();
            $table->string('code_commande'); // scrip generate
            $table->string('clientId');
            $table->dateTime('date_commande');
            $table->double('montant_total');
            $table->integer('modepaiementId')->nullable();
            $table->longText('adresse_livraison')->nullable();
            $table->longText('ville_livraison')->nullable();
            $table->string('statut_commande')->default('INITIALISATION'); // en cours, annulee, livree
            $table->boolean('is_deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('commandes');
    }
};
