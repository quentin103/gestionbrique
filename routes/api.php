<?php

use App\Http\Controllers\authController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// === *** unauthorized routes *** ===
Route::get('unauthorized', [authController::class, 'Unauthorized'])->name('unauthorizedRoute');
// === ***  authController routes *** ===
Route::post('/auth/login', [authController::class, 'login']);
Route::post('/auth/register', [authController::class, 'register']);

// === ***  middleware routes ***
Route::middleware(['middleware' => 'auth:sanctum'])->group(function () {
    //=== ***  authController routes *** ===
    Route::get('/auth/user',[authController::class, 'detailUser']);
    Route::post('/auth/logout', [authController::class, 'logout']);

    //=== ***  machine *** ===
    Route::resource('machine', \App\Http\Controllers\machineController::class)->only([
        'store','show','update','destroy','index'
    ]);
    //=== ***  materiel *** ===
    Route::resource('materiel', \App\Http\Controllers\materielController::class)->only([
        'store','show','update','destroy','index'
    ]);
    Route::post('/materiel/sortie/entrer/{id}', [\App\Http\Controllers\materielController::class, 'sortieEntrerMateriel_Store']);
    Route::get('/materiel/sortie/entrer/all', [\App\Http\Controllers\materielController::class, 'sortieEntrerMateriel_index']);
    Route::delete('/materiel/sortie/entrer/delete/{id}', [\App\Http\Controllers\materielController::class, 'sortieEntrerMateriel_Destroy']);
    //=== ***  brique *** ===
    Route::get('/brique/all', [\App\Http\Controllers\gestionBriqueController::class, 'indexBrique']);
    Route::get('/brique/show/{id}', [\App\Http\Controllers\gestionBriqueController::class, 'showBrique']);
    Route::post('/brique/create', [\App\Http\Controllers\gestionBriqueController::class, 'storeBrique']);
    Route::post('/brique/update/{id}', [\App\Http\Controllers\gestionBriqueController::class, 'updateBrique']);
    Route::delete('/brique/destroy/{id}', [\App\Http\Controllers\gestionBriqueController::class, 'destroyBrique']);
    //=== ***  briqueStock *** ===
    Route::get('/briqueStock/all', [\App\Http\Controllers\gestionBriqueController::class, 'indexBriqueStock']);
    Route::get('/briqueStock/show/{id}', [\App\Http\Controllers\gestionBriqueController::class, 'showBriqueStock']);
    Route::post('/briqueStock/create', [\App\Http\Controllers\gestionBriqueController::class, 'storeBriqueStock']);
    Route::post('/briqueStock/update/{id}', [\App\Http\Controllers\gestionBriqueController::class, 'updateBriqueStock']);
    Route::delete('/briqueStock/destroy/{id}', [\App\Http\Controllers\gestionBriqueController::class, 'destroyBriqueStock']);
    //=== ***  production *** ===
    Route::get('/production/all', [\App\Http\Controllers\productionController::class, 'indexProduction']);
    Route::get('/production/show/{id}', [\App\Http\Controllers\productionController::class, 'showProduction']);
    Route::post('/production/create', [\App\Http\Controllers\productionController::class, 'storeProduction']);
    Route::post('/production/update/{id}', [\App\Http\Controllers\productionController::class, 'updateProduction']);
    Route::delete('/production/destroy/{id}', [\App\Http\Controllers\productionController::class, 'destroyProduction']);


});

